<?xml version='1.0' encoding='utf-8'?>
<precice-configuration>
  <log-filter component="" target="debug" switch="off"/>
  <log-filter component="" target="info" switch="on"/>
  <log-output column-separator=" | " log-time-stamp="no" log-time-stamp-human-readable="yes" log-machine-name="no" log-message-type="no" log-trace="yes"/>
  <solver-interface dimensions="3">
    <data:vector name="Stresses"/>
    <data:vector name="Displacements"/>
    <data:scalar name="Acoustics_Density"/>
    <data:scalar name="Acoustics_Pressure"/>
    <data:scalar name="Acoustics_Velocity_X"/>
    <data:scalar name="Acoustics_Velocity_Y"/>
    <data:scalar name="Acoustics_Velocity_Z"/>
    <mesh name="Fluid_Nodes">
      <use-data name="Displacements"/>
    </mesh>
    <mesh name="Fluid_CellCenters">
      <use-data name="Stresses"/>
    </mesh>
    <mesh name="Structure_Nodes">
      <use-data name="Displacements"/>
    </mesh>
    <mesh name="Structure_CellCenters">
      <use-data name="Stresses"/>
    </mesh>
    <mesh name="Fluid_Acoustics">
      <use-data name="Acoustics_Density"/>
      <use-data name="Acoustics_Pressure"/>
      <use-data name="Acoustics_Velocity_X"/>
      <use-data name="Acoustics_Velocity_Y"/>
      <use-data name="Acoustics_Velocity_Z"/>
    </mesh>
    <mesh name="AcousticSurface_Ateles">
      <use-data name="Acoustics_Density"/>
      <use-data name="Acoustics_Pressure"/>
      <use-data name="Acoustics_Velocity_X"/>
      <use-data name="Acoustics_Velocity_Y"/>
      <use-data name="Acoustics_Velocity_Z"/>
    </mesh>
    <participant name="Fluid_Solver">
      <use-mesh name="Fluid_Nodes" provide="yes"/>
      <use-mesh name="Fluid_CellCenters" provide="yes"/>
      <use-mesh name="Fluid_Acoustics" provide="yes"/>
      <use-mesh name="Structure_Nodes" from="Structure_Solver"/>
      <use-mesh name="Structure_CellCenters" from="Structure_Solver"/>
      <write-data mesh="Fluid_CellCenters" name="Stresses"/>
      <write-data mesh="Fluid_Acoustics" name="Acoustics_Density"/>
      <write-data mesh="Fluid_Acoustics" name="Acoustics_Pressure"/>
      <write-data mesh="Fluid_Acoustics" name="Acoustics_Velocity_X"/>
      <write-data mesh="Fluid_Acoustics" name="Acoustics_Velocity_Y"/>
      <write-data mesh="Fluid_Acoustics" name="Acoustics_Velocity_Z"/>
      <read-data mesh="Fluid_Nodes" name="Displacements"/>
      <mapping:nearest-neighbor direction="write" from="Fluid_CellCenters" to="Structure_CellCenters" constraint="conservative" timing="initial"/>
      <mapping:nearest-neighbor direction="read" from="Structure_Nodes" to="Fluid_Nodes" constraint="consistent" timing="initial"/>
      <master:mpi-single/>
    </participant>
    <participant name="Structure_Solver">
      <use-mesh name="Structure_Nodes" provide="yes"/>
      <use-mesh name="Structure_CellCenters" provide="yes"/>
      <write-data mesh="Structure_Nodes" name="Displacements"/>
      <read-data mesh="Structure_CellCenters" name="Stresses"/>
      <master:mpi-single/>
    </participant>
    <participant name="Ateles_acoustic">
      <use-mesh name="AcousticSurface_Ateles" provide="yes"/>
      <use-mesh name="Fluid_Acoustics" from="Fluid_Solver"/>
      <read-data name="Acoustics_Density" mesh="AcousticSurface_Ateles"/>
      <read-data name="Acoustics_Pressure" mesh="AcousticSurface_Ateles"/>
      <read-data name="Acoustics_Velocity_X" mesh="AcousticSurface_Ateles"/>
      <read-data name="Acoustics_Velocity_Y" mesh="AcousticSurface_Ateles"/>
      <read-data name="Acoustics_Velocity_Z" mesh="AcousticSurface_Ateles"/>
      <mapping:nearest-neighbor direction="read" from="Fluid_Acoustics" to="AcousticSurface_Ateles" constraint="consistent" timing="initial"/>
      <master:mpi-single/>
    </participant>
    <m2n:sockets exchange-directory="../" from="Fluid_Solver" to="Structure_Solver" distribution-type="point-to-point"/>
    <m2n:sockets exchange-directory="../" from="Fluid_Solver" to="Ateles_acoustic" distribution-type="gather-scatter"/>
    <coupling-scheme:parallel-implicit>
      <timestep-length value="0.0001"/>
      <max-timesteps value="80"/>
      <participants first="Fluid_Solver" second="Structure_Solver"/>
      <exchange data="Stresses" from="Fluid_Solver" to="Structure_Solver" mesh="Structure_CellCenters"/>
      <exchange data="Displacements" from="Structure_Solver" to="Fluid_Solver" mesh="Structure_Nodes"/>
      <relative-convergence-measure limit="1e-05" suffices="no" data="Displacements" mesh="Structure_Nodes"/>
      <relative-convergence-measure limit="1e-05" suffices="no" data="Stresses" mesh="Structure_CellCenters"/>
      <max-iterations value="5"/>
      <extrapolation-order value="2"/>
      <post-processing:IQN-ILS>
        <data mesh="Structure_Nodes" name="Displacements"/>
        <data mesh="Structure_CellCenters" name="Stresses"/>
        <initial-relaxation value="0.001"/>
        <max-used-iterations value="200"/>
        <timesteps-reused value="8"/>
        <filter limit="1e-08" type="QR1"/>
        <preconditioner type="residual-sum"/>
      </post-processing:IQN-ILS>
    </coupling-scheme:parallel-implicit>
    <coupling-scheme:parallel-explicit>
      <participants first="Fluid_Solver" second="Ateles_acoustic"/>
      <max-timesteps value="80"/>
      <timestep-length value="0.0001"/>
      <exchange data="Acoustics_Density" from="Fluid_Solver" to="Ateles_acoustic" mesh="Fluid_Acoustics" initialize="yes"/>
      <exchange data="Acoustics_Pressure" from="Fluid_Solver" to="Ateles_acoustic" mesh="Fluid_Acoustics" initialize="yes"/>
      <exchange data="Acoustics_Velocity_X" from="Fluid_Solver" to="Ateles_acoustic" mesh="Fluid_Acoustics" initialize="yes"/>
      <exchange data="Acoustics_Velocity_Y" from="Fluid_Solver" to="Ateles_acoustic" mesh="Fluid_Acoustics" initialize="yes"/>
      <exchange data="Acoustics_Velocity_Z" from="Fluid_Solver" to="Ateles_acoustic" mesh="Fluid_Acoustics" initialize="yes"/>
    </coupling-scheme:parallel-explicit>
  </solver-interface>
</precice-configuration>
