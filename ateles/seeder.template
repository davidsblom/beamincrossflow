-- Use this file as template. Do not modify this file for running some testcases

<%
  import math
  a = int( minlevel )
  a = max( a, 7 )
%>

outputname = 'acoustic'
outputpreview = true
folder = 'mesh_3d_acousticI_per/'

max_num_y=2
max_num_x=6
x=6
y=10
z=2
x_start = 0
y_start = 2
z_start = 0
dx=y/max_num_y

nLength_bnd = max_num_x+2
minlevel = ${a}
--minlevel = math.ceil(math.log(nLength_bnd)/math.log(2))
--cubeLength = (2^minlevel)*dx
cubeLength = 32

--minlevel = 5
--maxlevel = 5
--flowlevel=6

--cubeLength = 44.8
--flow_elemsize = 0.35
elemsize = cubeLength/(2^minlevel)

-- boundingbox: two entries: origin and length in this
-- order, if no keys are used
bounding_cube = {
                 --origin = { -elemsize, -5*elemsize, -elemsize },
                 origin = { cubeLength/(-2.0), cubeLength/(-2.0), cubeLength/(-2.0)} ,
                 --origin = { -4, -4, -4} ,
                 --origin = {-flow_elemsize,cubeLength/(-2.0),cubeLength/(-2.0)},
                 length = cubeLength
                }

geom_level=minlevel
epslevel = minlevel+5
--plate_level = minlevel

-- smallness parameter
eps = bounding_cube.length/(2^epslevel)

-- the element size around the plate
--smalleps = bounding_cube.length/(2^plate_level)

-- the element size of the largest elements (belonging to minlevel)
largeeps = bounding_cube.length/(2^minlevel)

spatial_object =
                {
                  {
                  attribute = {
                               kind = 'seed'
                              },
                  geometry = {
                                   {
                                   kind = 'canoND',
                                   object =
                                            {
                                             origin = { 3+eps, 3+eps, 1+eps},
                                             --origin = { eps, eps, eps},
                                             --origin = { 3+eps, 4+eps, 0.0-eps},
                                            -- origin = {x_start+elemsize, y_start+elemsize, z_start+eps},
                                            },
                                 },
                           }
                  },
                  -- periodic boundary in x direction
                 {
                   attribute = {
                                   kind = 'periodic',
                                   level = minlevel,
                                 },
                     geometry = {
                                  kind = 'periodic',
                                  object = {
                                              plane1={
                                                       vec= {
                                                              {0.0,cubeLength,0.0},
                                                              {0.0,0.0,cubeLength},
                                                            },
                                                       origin={
                                                                x+eps,
                                                                y_start-eps,
                                                                -eps,
                                                              },
                                                     },
                                                plane2={
                                                        vec= {
                                                                {0.0,0.0,cubeLength},
                                                                {0.0,cubeLength,0.0},
                                                             },
                                                        origin={
                                                                  -eps,
                                                                  y_start-eps,
                                                                  -eps
                                                                },
                                                        },
                                              },
                                },
                  },
--                  -- periodic boundary in y direction
--                 {
--                   attribute = {
--                                   kind = 'periodic',
--                                   level = minlevel,
--                                },
--                     geometry = {
--                                  kind = 'periodic',
--                                  object = {
--                                              plane1={
--                                                      vec= {
--                                                             {cubeLength,0.0,0.0},
--                                                             {0.0,0.0,cubeLength},
--                                                           },
--                                                      origin={
--                                                              -eps,
--                                                              -eps,
--                                                              2-eps,
--                                                             },
--                                                      },
--                                               plane2={
--                                                       vec= {
--                                                              {0.0,0.0,cubeLength},
--                                                              {cubeLength,0.0,0.0},
--                                                            },
--                                                      origin={
--                                                               -eps,
--                                                               2+eps,
--                                                               2-eps
--                                                             },
--                                                       },
--                                              },
--                                },
--                 },
                  -- periodic boundary in z direction
                 {
                   attribute = {
                                   kind = 'periodic',
                                   level = minlevel,
                                },
                     geometry = {
                                  kind = 'periodic',
                                  object = {
                                              plane1={
                                                      vec= {
                                                             {cubeLength,0.0,0.0},
                                                             {0.0,cubeLength,0.0},
                                                           },
                                                      origin={
                                                              -eps,
                                                              y_start-eps,
                                                              -eps,
                                                             },
                                                      },
                                               plane2={
                                                       vec= {
                                                                {cubeLength,0.0,0.0},
                                                                {0.0,cubeLength,0.0},
                                                            },
                                                         origin={ -eps,
                                                                  y_start-eps,
                                                                  z+eps
                                                                },
                                                       },
                                              },
                                },
                 },

--                  -- eastern acoustic bnd
--                  {
--                   attribute = {
--                                 kind = 'boundary',
--                                 label = 'acoustic_East',
--                                 level = geom_level,
--                               },
--                   geometry = {
--                                 kind = 'canoND',
--                                 object =
--                                    {
--                                     vec= {
--                                            {0.0,cubeLength,0.0},
--                                            {0.0,0.0,cubeLength},
--                                            --{0.0,4-2*eps,0.0},
--                                            --{0.0,0.0,elemsize-2*eps},
--                                          },
--                                     origin={
--                                              6+eps,
--                                              2-eps,
--                                              -0.5-eps,
--                                            },
--                                          },
--                              }
--                  },
--                  -- western outflow bnd
--                  {
--                   attribute = {
--                                 kind = 'boundary',
--                                 label = 'acoustic_West',
--                                 level = geom_level,
--                               },
--                   geometry = {
--                                 kind = 'canoND',
--                                 object =
--                                          {
--                                           vec= {
--                                                   {0.0,cubeLength,0.0},
--                                                   {0.0,0.0,cubeLength},
--                                                   --{0.0,y+2*eps,0.0},
--                                                   --{0.0,0.0,elemsize-2*eps},
--                                                },
--                                           origin={
--                                                     -eps,
--                                                     2-eps,
--                                                     -0.5-eps },
--                                          },
--                              }
--                  },
                  -- southern slip bnd
                  {
                   attribute = {
                                 kind = 'boundary',
                                 label = 'flow', --acoustic_South',
                                 level = geom_level,
                               },
                   geometry = {
                                 kind = 'canoND',
                                 object =
                                          {
                                           vec= {
                                                  {cubeLength,0.0,0.0},
                                                  {0.0,0.0,cubeLength},
                                                  --{6+2*eps,0.0,0.0},
                                                  --{0.0, 0.0, elemsize-2*eps},
                                                },
                                           origin={
                                                   -eps,
                                                   y_start-eps,
                                                   -eps,
                                                  },
                                          },
                              }
                  },
                  -- northern slip bnd
                  {
                   attribute = {
                                 kind = 'boundary',
                                 label = 'acoustic_North',
                                 level = geom_level,
                               },
                   geometry = {
                                 kind = 'canoND',
                                 object =
                                          {
                                           vec= {
                                                  {cubeLength,0.0,0.0},
                                                  {0.0,0.0,cubeLength},
                                                  --{x+2*eps,0.0,0.0},
                                                  --{0.0,0.0,elemsize-2*eps},
                                                },
                                           origin={
                                                   -eps,
                                                   y+eps,
                                                   -eps,
                                                  },
                                          },
                              }
                  },
--                  -- the euler interface south
--                  {
--                   attribute = {
--                                 kind = 'boundary',
--                                 label = 'euler',
--                                 level = geom_level,
--                               },
--                   geometry = {
--                                 kind = 'canoND',
--                                 object =
--                                          {
--                                           vec= {
--                                                  {4*elemsize-2*eps,0.0,0.0},
--                                                  {0.0,4*elemsize-2*eps,0.0},
--                                                  {0.0,0.0,elemsize},
--                                                },
--                                           origin={
--                                                   eps,
--                                                   eps,
--                                                   0.0,
--                                                  },
--                                          },
--                              }
--                  },
--               --boundary in z direction
--                 {
--                  attribute = {
--                                kind = 'boundary',
--                                label = 'wallBottom',
--                                level = minlevel,
--                              },
--                  geometry = {
--                                kind = 'canoND',
--                                object =
--                                         {
--                                            vec= {
--                                                   {cubeLength,0.0,0.0},
--                                                   {0.0,cubeLength,0.0},
--                                                   --{6,0.0,0.0},
--                                                   --{0.0,4,0.0},
--                                                 },
--                                            origin={-eps,
--                                                    2-eps,
--                                                    -eps},
--                                         }
--                              }
--                 },
--                 {
--                  attribute = {
--                                kind = 'boundary',
--                                label = 'wallTop',
--                                level = minlevel,
--                              },
--                  geometry = {
--                                kind = 'canoND',
--                                object =
--                                        {
--                                          vec= {
--                                                   {cubeLength,0.0,0.0},
--                                                   {0.0,cubeLength,0.0},
--                                                  -- {6,0.0,0.0},
--                                                  -- {0.0,4,0.0},
--                                               },
--                                            origin={ -eps,
--                                                     2-eps,
--                                                     1.0+eps},
--                                         },
--                              }
--                 },
}
