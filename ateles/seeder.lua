-- Use this file as template. Do not modify this file for running some testcases

outputname = 'acoustic'
outputpreview = true
folder = 'mesh/'

max_num_y=2
max_num_x=6
x=6
y=10
z=2
x_start = 0
y_start = 2
z_start = 0
dx=y/max_num_y

nLength_bnd = max_num_x+2
minlevel = 7
cubeLength = 32

elemsize = cubeLength/(2^minlevel)

bounding_cube = {
                 origin = { cubeLength/(-2.0), cubeLength/(-2.0), cubeLength/(-2.0)} ,
                 --origin = { -4, -4, -4} ,
                 --origin = {-flow_elemsize,cubeLength/(-2.0),cubeLength/(-2.0)},
                 length = cubeLength
                }

geom_level=minlevel
epslevel = minlevel+5

-- smallness parameter
eps = bounding_cube.length/(2^epslevel)

-- the element size of the largest elements (belonging to minlevel)
largeeps = bounding_cube.length/(2^minlevel)

spatial_object =
                {
                  {
                  attribute = {
                               kind = 'seed'
                              },
                  geometry = {
                                   {
                                   kind = 'canoND',
                                   object =
                                            {
                                             origin = { 3+eps, 3+eps, 1+eps},
                                             --origin = { eps, eps, eps},
                                             --origin = { 3+eps, 4+eps, 0.0-eps},
                                            -- origin = {x_start+elemsize, y_start+elemsize, z_start+eps},
                                            },
                                 },
                           }
                  },
                  -- periodic boundary in x direction
                 {
                   attribute = {
                                   kind = 'periodic',
                                   level = minlevel,
                                 },
                     geometry = {
                                  kind = 'periodic',
                                  object = {
                                              plane1={
                                                       vec= {
                                                              {0.0,cubeLength,0.0},
                                                              {0.0,0.0,cubeLength},
                                                            },
                                                       origin={
                                                                x+eps,
                                                                y_start-eps,
                                                                -eps,
                                                              },
                                                     },
                                                plane2={
                                                        vec= {
                                                                {0.0,0.0,cubeLength},
                                                                {0.0,cubeLength,0.0},
                                                             },
                                                        origin={
                                                                  -eps,
                                                                  y_start-eps,
                                                                  -eps
                                                                },
                                                        },
                                              },
                                },
                  },
                  -- periodic boundary in z direction
                 {
                   attribute = {
                                   kind = 'periodic',
                                   level = minlevel,
                                },
                     geometry = {
                                  kind = 'periodic',
                                  object = {
                                              plane1={
                                                      vec= {
                                                             {cubeLength,0.0,0.0},
                                                             {0.0,cubeLength,0.0},
                                                           },
                                                      origin={
                                                              -eps,
                                                              y_start-eps,
                                                              -eps,
                                                             },
                                                      },
                                               plane2={
                                                       vec= {
                                                                {cubeLength,0.0,0.0},
                                                                {0.0,cubeLength,0.0},
                                                            },
                                                         origin={ -eps,
                                                                  y_start-eps,
                                                                  z+eps
                                                                },
                                                       },
                                              },
                                },
                 },
                  -- southern slip bnd
                  {
                   attribute = {
                                 kind = 'boundary',
                                 label = 'flow', --acoustic_South',
                                 level = geom_level,
                               },
                   geometry = {
                                 kind = 'canoND',
                                 object =
                                          {
                                           vec= {
                                                  {cubeLength,0.0,0.0},
                                                  {0.0,0.0,cubeLength},
                                                  --{6+2*eps,0.0,0.0},
                                                  --{0.0, 0.0, elemsize-2*eps},
                                                },
                                           origin={
                                                   -eps,
                                                   y_start-eps,
                                                   -eps,
                                                  },
                                          },
                              }
                  },
                  -- northern slip bnd
                  {
                   attribute = {
                                 kind = 'boundary',
                                 label = 'acoustic_North',
                                 level = geom_level,
                               },
                   geometry = {
                                 kind = 'canoND',
                                 object =
                                          {
                                           vec= {
                                                  {cubeLength,0.0,0.0},
                                                  {0.0,0.0,cubeLength},
                                                  --{x+2*eps,0.0,0.0},
                                                  --{0.0,0.0,elemsize-2*eps},
                                                },
                                           origin={
                                                   -eps,
                                                   y+eps,
                                                   -eps,
                                                  },
                                          },
                              }
                  }
}
