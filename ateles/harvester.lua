name = 'plate'
 
-- define the input
input = {
          --mesh = './mesh_2d_acoustic/',
          --mesh = './mesh_3d_acousticI_per/',
          read = './debug/0_restart_ProtoData_header_0.lua',
         
  }

-- define the output
output = {  -- some general information
            --folder = './mesh_2d_acoustic/',     -- Output location 
            folder = './debug/',     -- Output location 
            --folder = './mesh_3d_acousticI_per/',     -- Output location 

           {

    
            format = 'VTU',   -- Output format 

            binary = true,
            vrtx = {           -- when this table is defined set use_vrtx = .true.
--                     updated_vertices  = 'prefix_of_new_vertices',            -- when this is given set  new_vrtx = .true.
--                     old_vertices  = 'prefix_of_old_vertices',                -- when this is given set  old_vrtx = .true.
--                     dump_vertices = 'prefix_of_where_to_dump_vertices'       -- when this is given set dump_vrtx = .true.
                   } 
           }    

         }
