require('acoustic_3d')
name = 'acoustic'

 
input = {
         --read = './restart/simulation_lastHeader.lua',
         read = 'restart/acoustic/simulation_lastHeader.lua',
         
         subsampling = {
                         --levels = math.floor(math.log(spatial_order)/math.log(2)), 
                         levels = 2,
                         projection = 'QLegendrePoint',
                       },

  }


output = {  
            --folder = './harvest/', 
            folder = 'harvest_cons_VelX/acoustic/', 

           {

    
            format = 'VTU',

            binary = true,
            requestedData = {
                             variable = {
                                          { name = 'density', },
                                          { name = 'momentum',},
                                          { name = 'energy',  },
                                          --{ name = 'vorticity',  },
                                          { name = 'pressure', ncomponents = 1 },
                                          --{ name = 'temperature', ncomponents = 1 },
                                          { name = 'velocity', ncomponents = 3 },
                                        },
                            },
            vrtx = {
                   },
           }    

         }

