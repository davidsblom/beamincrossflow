-- Configuration file for Ateles --

<%
  import math
  a = int( degree )
  a = max( a, 1 )
  b = int( nbTimeSteps )
  b = max( b, 1 )
%>

require 'seeder'
degree=${a}
spatial_order = degree+1
logging={level=5}

-- global simulation options
simulationname = 'gauss_acoustic_modg'             -- the name of the simualtion
sim_control = {
              time_control  = {
                                min = 0.0,
                                max = {iter=${b}},
                                interval = {iter=1},
                              },
             }

check = {
          interval = 1,
        }

--...Info for precice
precice = {
           accessor = 'Ateles_acoustic',
           configFile ='precice_config.xml',
          }


-- Mesh definitions --
mesh = './mesh_3d_acousticI_per/'


-- Restart settings
restart = {
            write = './restart/acoustic/',
            -- temporal definition of restart write
            time_control = {
                            min = 0,
                            max = {iter=sim_control.time_control.max.iter},
                            interval = {iter=1000}
                           },
          }


-- timing settings (i.e. output for performance measurements, this table is otional)
timing = {
          folder = './',                  -- the folder for the timing results
          filename = 'timing.res'         -- the filename of the timing results
         }


density= 1.0
velX = 2.3
pressure = 100.0

-- Equation definitions --
equation = {
             name   = 'linearEuler',
             isen_coef = 1.4,
             background = {
                 density = density,
                 velocityX = velX,
                 velocityY = 0.0,
                 velocityZ = 0.0,
                 pressure = pressure,
                 }
           }

-- Scheme definitions --
scheme = {
    -- the spatial discretization scheme
    spatial =  {
               name = 'modg',            -- we use the modal discontinuous Galerkin scheme
               m = degree,
               },
    -- the temporal discretization scheme
    temporal = {
                name = 'explicitSSPRungeKutta',  -- we use ssp explicit runge kutta in time
                steps = 2,                       -- we have a 2 stage
                control = {                      -- how to control the timestep
                           name = 'cfl',         -- the name of the timestep control mechanism
                           cfl = 0.8            -- Courant Friedrichs Lewy number
                          }
               }
}


-- This is a very simple example to define constant boundary condtions.
initial_condition = { density = 0.0,
                      pressure = 0.0,
                      velocityX = 0.0,
                      velocityY = 0.0,
                      velocityZ = 0.0,
                    }


-- ...the general projection table
projection = {
              kind = 'fpt',  -- 'fpt' or 'l2p', default 'l2p'
              factor = 1.0,          -- dealising factpr for fpt, oversampling factor for l2p, float, default 1.0
             }

-- Boundary definitions
boundary_condition = {
                       {
                         label = 'flow',
                         kind = 'precice',
                         precice_mesh = 'AcousticSurface_Ateles',
                         exchange_data_read =  {'Acoustics_Density',
                                                'Acoustics_Velocity_X',
                                                'Acoustics_Velocity_Y',
                                                'Acoustics_Velocity_Z',
                                                'Acoustics_Pressure'
                                                }
                       }
                       ,
                       {
                         label = 'acoustic_North',
                          kind = 'primitives',
                          density = 0.0,
                          v_x = 0.0,
                          v_y = 0.0,
                          v_z = 0.0,
                          pressure = 0.0
                       }
                     }
