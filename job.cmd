#!/bin/bash

#@ wall_clock_limit = 00:30:00
#@ output = job.out
#@ error = job.err
#@ job_type = MPICH
#@ class = test
#@ island_count = 1
#@ node = 3
#@ tasks_per_node = 16
#@ network.MPI = sn_all,not_shared,us
#@ initialdir = /home/hpc/pr84va/di29tug/beamInCrossFlow
#@ notification=always
#@ notify_user=d.s.blom@tudelft.nl
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh

module unload gcc
module unload mpi.ibm
module unload mpi.intel
module unload mkl
module load cmake
module load hwloc
module load fftw
module load bison/2.7
module load python
module unload gcc
module load gcc/4.7
module load mpi.intel/4.1_gcc
module unload gcc
module load gcc/4.9
module load mkl/11.3_gcc
module unload gcc
module load gcc/5
module load valgrind

export SUBJOB

# Settings OpenFOAM
export WM_MPLIB=INTELMPI
export HWLOC_SYSTEM=1
export HWLOC_DIR=$HWLOC_BASE
export HWLOC_BIN_DIR=$HWLOC_DIR/bin
export CMAKE_SYSTEM=1
export CMAKE_DIR=$CMAKE_BASE
export CMAKE_BIN_DIR=$CMAKE_DIR/bin
export BISON_SYSTEM=1
export BISON_DIR=$BISON_BASE
export BISON_BIN_DIR=$BISON_DIR/bin
export FOAM_INST_DIR=~/repositories/openfoam
source ${FOAM_INST_DIR}/foam-extend-3.2/etc/bashrc

# precice
export PATH=~/repositories/openfoam/davidblom-3.1-ext/src/thirdParty/precice/build/last:$PATH

# ateles
export PATH=~/repositories/ateles/build:$PATH

./Allrun_supermuc

echo "JOB is run"
