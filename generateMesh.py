#!/usr/bin/env python

import mako.template

factor = 1

template = mako.template.Template( filename = 'fluid/constant/polyMesh/blockMeshDict.template' )

f = open( 'fluid/constant/polyMesh/blockMeshDict', 'w' )
output = template.render( factor = factor )
f.write( output )
f.close()

template = mako.template.Template( filename = 'solid/constant/polyMesh/blockMeshDict.template' )

f = open( 'solid/constant/polyMesh/blockMeshDict', 'w' )
output = template.render( factor = factor )
f.write( output )
f.close()
